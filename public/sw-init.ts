if('serviceWorker' in navigator) {

    window.addEventListener('load', () => {
        navigator.serviceWorker.register('./serviceworker.js')
        .then((reg) => console.log('Success:', reg.scope))
        .catch((error) => console.log('Error:', error));
    });
   
    // === Custom button event to trigger to install app ===
    const btnInstallTheApp = document.querySelector('#btn_install_the_app')! as HTMLDivElement;
    let deferredPrompt;
    
    window.addEventListener('beforeinstallprompt', (e) => {
        deferredPrompt = e;
        // Show install button
        btnInstallTheApp.classList.remove('hide_btn_install');
    });

    window.addEventListener('offline', () => {
        // Hide install button when offline
        btnInstallTheApp.classList.add('hide_btn_install');
    });

    window.addEventListener('online', () => {
        // Show install button if app has NOT been installed yet
        if(deferredPrompt !== undefined) {
            // Show install button
            btnInstallTheApp.classList.remove('hide_btn_install');
        }
    });

    btnInstallTheApp.addEventListener('click', async () => {
        if (deferredPrompt !== null) {
            deferredPrompt.prompt();
            const { outcome } = await deferredPrompt.userChoice;
            if (outcome === 'accepted') {
                deferredPrompt = null;
                // Hide install button if app has been installed
                btnInstallTheApp.classList.add('hide_btn_install');
                /**
                 * Reload page so that install button is considered
                 * hidden when app has been installed
                */
                window.location.reload();
            }
        }
    });
    // === End of Custom button event to trigger to install app ===

}