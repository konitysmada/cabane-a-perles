var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
if ('serviceWorker' in navigator) {
    window.addEventListener('load', function () {
        navigator.serviceWorker.register('./serviceworker.js')
            .then(function (reg) { return console.log('Success:', reg.scope); })["catch"](function (error) { return console.log('Error:', error); });
    });
    // === Custom button event to trigger to install app ===
    var btnInstallTheApp_1 = document.querySelector('#btn_install_the_app');
    var deferredPrompt_1;
    window.addEventListener('beforeinstallprompt', function (e) {
        deferredPrompt_1 = e;
        // Show install button
        btnInstallTheApp_1.classList.remove('hide_btn_install');
    });
    window.addEventListener('offline', function () {
        // Hide install button when offline
        btnInstallTheApp_1.classList.add('hide_btn_install');
    });
    window.addEventListener('online', function () {
        // Show install button if app has NOT been installed yet
        if (deferredPrompt_1 !== undefined) {
            // Show install button
            btnInstallTheApp_1.classList.remove('hide_btn_install');
        }
    });
    btnInstallTheApp_1.addEventListener('click', function () { return __awaiter(_this, void 0, void 0, function () {
        var outcome;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(deferredPrompt_1 !== null)) return [3 /*break*/, 2];
                    deferredPrompt_1.prompt();
                    return [4 /*yield*/, deferredPrompt_1.userChoice];
                case 1:
                    outcome = (_a.sent()).outcome;
                    if (outcome === 'accepted') {
                        deferredPrompt_1 = null;
                        // Hide install button if app has been installed
                        btnInstallTheApp_1.classList.add('hide_btn_install');
                        /**
                         * Reload page so that install button is considered
                         * hidden when app has been installed
                        */
                        window.location.reload();
                    }
                    _a.label = 2;
                case 2: return [2 /*return*/];
            }
        });
    }); });
    // === End of Custom button event to trigger to install app ===
}
