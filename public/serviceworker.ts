const CACHE_NAME = 'version-1';
const urlsToCache = ['index.html', 'offline.html'];

const selfSw = this;

// Install the Service Worker
selfSw.addEventListener('install', (e: any) => {
    e.waitUntil(
        caches.open(CACHE_NAME)
        .then((cache) => {
            console.log('Opened cache');
            return cache.addAll(urlsToCache);
        })
    )
});

// Listen for requests
selfSw.addEventListener('fetch', (e: any) => {
    e.respondWith(
        caches.match(e.request)
        .then(() => {
            return fetch(e.request)
            .catch(() => caches.match('offline.html'))
        })
    )
});

// Activate the Service Worker
selfSw.addEventListener('activate', (e: any) => {
    const cacheWhiteList: string[] = [];
    cacheWhiteList.push(CACHE_NAME);
    e.waitUntil(
        caches.keys().then((cacheNames) => Promise.all(
            cacheNames.map((cacheName) => {
                if(!cacheWhiteList.includes(cacheName)) {
                    return caches.delete(cacheName);
                }
            })
        ))
    )
});

/*
//Deletion should only occur at the activate event
selfSw.addEventListener('activate', (e: any) => {
    var cacheKeeplist = [CACHE_NAME];
    e.waitUntil(
        caches.keys().then( keyList => {
            return Promise.all(keyList.map( key => {
                if (cacheKeeplist.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }));
        })
        .then(selfSw.clients.claim()));
});
*/