var CACHE_NAME = 'version-1';
var urlsToCache = ['index.html', 'offline.html'];
var selfSw = this;
// Install the Service Worker
selfSw.addEventListener('install', function (e) {
    e.waitUntil(caches.open(CACHE_NAME)
        .then(function (cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
    }));
});
// Listen for requests
selfSw.addEventListener('fetch', function (e) {
    e.respondWith(caches.match(e.request)
        .then(function () {
        return fetch(e.request)["catch"](function () { return caches.match('offline.html'); });
    }));
});
// Activate the Service Worker
selfSw.addEventListener('activate', function (e) {
    var cacheWhiteList = [];
    cacheWhiteList.push(CACHE_NAME);
    e.waitUntil(caches.keys().then(function (cacheNames) { return Promise.all(cacheNames.map(function (cacheName) {
        if (!cacheWhiteList.includes(cacheName)) {
            return caches["delete"](cacheName);
        }
    })); }));
});
/*
//Deletion should only occur at the activate event
selfSw.addEventListener('activate', (e: any) => {
    var cacheKeeplist = [CACHE_NAME];
    e.waitUntil(
        caches.keys().then( keyList => {
            return Promise.all(keyList.map( key => {
                if (cacheKeeplist.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }));
        })
        .then(selfSw.clients.claim()));
});
*/
