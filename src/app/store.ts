import { configureStore } from '@reduxjs/toolkit';
import internetConnectionReducer from '../features/internetConnectionSlice';
import serverUpdatedReducer from '../features/serverUpdatedSlice';
import websiteFirstTimeLoadedReducer from '../features/websiteFirstTimeLoadedSlice';
import browserPathnameReducer from '../features/browserPathnameSlice';
import itemsReducer from '../features/itemsSlice';
import videoPlayerReducer from '../features/videoPlayerSlice';

export const store = configureStore({
	reducer: {
		internetConnection: internetConnectionReducer,
		serverUpdated: serverUpdatedReducer,
		websiteFirstTimeLoaded: websiteFirstTimeLoadedReducer,
		browserPathname: browserPathnameReducer,
		items: itemsReducer,
		videoPlayer: videoPlayerReducer
	}
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;