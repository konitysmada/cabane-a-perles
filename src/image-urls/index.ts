import logoImgUrl from '../images/logo-main.svg';
import bgCatalogBgUrl1 from '../images/bg-catalogue1.jpg';
import bgCatalogBgUrl2 from '../images/bg-catalogue2.jpg';
import bgCatalogBgUrl3 from '../images/bg-catalogue3.jpg';
import productImgUrl from '../images/bague2.jpg';

export const getLogoImgUrl = () => logoImgUrl;
export const getCatalogBgImgUrl1 = () => bgCatalogBgUrl1;
export const getCatalogBgImgUrl2 = () => bgCatalogBgUrl2;
export const getCatalogBgImgUrl3 = () => bgCatalogBgUrl3;
export const getProductImgUrl = () => productImgUrl;