import axios from "axios";

export default axios.create({
    baseURL: 'https://www.lacabaneaperles.fr/wp-json/wp/v2'
});