export enum PageUrl {
    HomeUrl = '/',
    TutorialsUrl = '/tutos',
    IdeasUrl = '/idees'
}

export enum VideoControls {
    PlayIconClass = 'play-ic',
    PauseIconClass = 'pause-ic'
}