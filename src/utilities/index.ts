import { useState, useEffect } from 'react';

export const GetNetworkStatus = (): boolean => {
    const [isOnline, setIsOnline] = useState<boolean>(navigator.onLine);

    useEffect(() => {

        const setToOnline = () => {
            setIsOnline(true);
        }
        const setToOffline = () => {
            setIsOnline(false);
        }
        window.addEventListener('online', setToOnline);
        window.addEventListener('offline', setToOffline);
        
        return () => {
            window.removeEventListener('online', setToOnline);
            window.removeEventListener('offline', setToOffline);
        }

    }, []);

    return isOnline;
}