import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface videoPlayerType {
    isPlaying: boolean;
    playedTime: number;
    isManuallySeeking: boolean;
    displayedTotalDuration: string;
    videoEnded: boolean;
    videoIsBuffering: boolean;
}

const initialState: videoPlayerType = {
    isPlaying: false,
    playedTime: 0,
    isManuallySeeking: false,
    displayedTotalDuration: '00:00',
    videoEnded: false,
    videoIsBuffering: false,
}

export const videoPlayerSlice = createSlice({
	name: 'videoPlayer',
	initialState,
    reducers: {
        setVideoIsPlaying: (state, action: PayloadAction<boolean>) => {
            state.isPlaying = action.payload;
        },
        setPlayedTime: (state, action: PayloadAction<number>) => {
            state.playedTime = action.payload;
        },
        setIsManuallySeeking: (state, action: PayloadAction<boolean>) => {
            state.isManuallySeeking = action.payload;
        },
        setDisplayedTotalDuration: (state, action: PayloadAction<string>) => {
            state.displayedTotalDuration = action.payload;
        },
        setVideoEnded: (state, action: PayloadAction<boolean>) => {
            state.videoEnded = action.payload;
        },
        setVideoIsBuffering: (state, action: PayloadAction<boolean>) => {
            state.videoIsBuffering = action.payload;
        },
    }
});

export const {
    setVideoIsPlaying,
    setPlayedTime,
    setIsManuallySeeking,
    setDisplayedTotalDuration,
    setVideoEnded,
    setVideoIsBuffering,
} = videoPlayerSlice.actions;
export default videoPlayerSlice.reducer;