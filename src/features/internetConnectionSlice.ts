import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface internetConnectionType {
    value: boolean;
}

const initialState: internetConnectionType = {
    value: navigator.onLine,
}

export const internetConnectionSlice = createSlice({
	name: 'internetConnection',
	initialState,
    reducers: {
        setInternetConnection: (state, action: PayloadAction<boolean>) => {
            state.value = action.payload;
        },
    }
});

export const { setInternetConnection } = internetConnectionSlice.actions;
export default internetConnectionSlice.reducer;