import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface browserPathnameType {
    value: string;
    pageId: string;
}

const initialState: browserPathnameType = {
    value: window.location.pathname,
    pageId: '',
}

export const browserPathnameSlice = createSlice({
	name: 'browserPathname',
	initialState,
    reducers: {
        setBrowserPathname: (state, action: PayloadAction<string>) => {
            state.value = action.payload;
        },
        setPageId: (state, action: PayloadAction<string>) => {
            state.pageId = action.payload;
        },
    }
});

export const { setBrowserPathname, setPageId } = browserPathnameSlice.actions;
export default browserPathnameSlice.reducer;