import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface serverUpdateType {
    value: boolean;
}

const initialState: serverUpdateType = {
    value: false,
}

export const serverUpdatedSlice = createSlice({
	name: 'serverUpdated',
	initialState,
    reducers: {
        setServerUpdated: (state, action: PayloadAction<boolean>) => {
            state.value = action.payload;
        },
    }
});

export const { setServerUpdated } = serverUpdatedSlice.actions;
export default serverUpdatedSlice.reducer;