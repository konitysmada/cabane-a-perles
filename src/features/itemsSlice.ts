import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface itemsType {
    tutos: object[];
    idees: object[];
}

const initialState: itemsType = {
    tutos: [],
    idees: [],
}

export const itemsSlice = createSlice({
	name: 'items',
	initialState,
    reducers: {
        setTutos: (state, action: PayloadAction<object[]>) => {
            state.tutos = action.payload;
        },
        setIdees: (state, action: PayloadAction<object[]>) => {
            state.idees = action.payload;
        },
    }
});

export const { setTutos, setIdees } = itemsSlice.actions;
export default itemsSlice.reducer;