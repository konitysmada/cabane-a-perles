import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface websiteFirstTimeLoadedType {
    value: boolean;
}

const initialState: websiteFirstTimeLoadedType = {
    value: true,
}

export const websiteFirstTimeLoadedSlice = createSlice({
	name: 'websiteFirstTimeLoaded',
	initialState,
    reducers: {
        setWebsiteFirstTimeLoaded: (state, action: PayloadAction<boolean>) => {
            state.value = action.payload;
        },
    }
});

export const { setWebsiteFirstTimeLoaded } = websiteFirstTimeLoadedSlice.actions;
export default websiteFirstTimeLoadedSlice.reducer;