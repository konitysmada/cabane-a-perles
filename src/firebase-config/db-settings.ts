import firebase from 'firebase/app';

export default firebase.initializeApp({
    apiKey: "AIzaSyBCxT2kEs3PgXdx1KVRn92rU8C07CXhgCc",
    authDomain: "cabane-a-perles-af0fa.firebaseapp.com",
    projectId: "cabane-a-perles-af0fa",
    storageBucket: "cabane-a-perles-af0fa.appspot.com",
    messagingSenderId: "826965065939",
    appId: "1:826965065939:web:e6c7d5e94385d9bfe59426"
});