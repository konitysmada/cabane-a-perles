import React from 'react';
import '../css/Home.css';
import MenuItemBlock from './MenuItemBlock';
import { PageUrl } from '../enums';

const Home: React.FC = () => {
    return (
        <div className="home-pg-wrpr">
            <div className="pg-mnu-itm-lst-wrpr">
                <MenuItemBlock
                    text="Tutos"
                    columnType="two-col-item"
                    pagePath={PageUrl.TutorialsUrl}
                />
                <MenuItemBlock
                    text="Idées"
                    columnType="two-col-item"
                    pagePath={PageUrl.IdeasUrl}
                />
                {/*<MenuItemBlock
                    text="Nouveautés"
                    columnType="two-col-item"
                    pagePath={PageUrl.CatalogUrl}
                />
                <MenuItemBlock
                    text="Tutos"
                    columnType="two-col-item"
                    pagePath={PageUrl.CatalogUrl}
                />*/}
            </div>
        </div>
    )
}

export default Home;