import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../app/store';
import { setServerUpdated } from '../features/serverUpdatedSlice';
import { setWebsiteFirstTimeLoaded } from '../features/websiteFirstTimeLoadedSlice';
import '../css/Popup.css';

const Popup: React.FC = () => {

    const dispatch = useDispatch();
    const serverUpdated = useSelector((state: RootState) => state.serverUpdated.value);
    const websiteFirstTimeLoaded = useSelector((state: RootState) => state.websiteFirstTimeLoaded.value);

    useEffect(() => {
        if(websiteFirstTimeLoaded) {
            closePopup();
        }
    });

    const closePopup = () => {
        dispatch(setServerUpdated(false));
        dispatch(setWebsiteFirstTimeLoaded(false));
    }

    const getServerUpdates = () => {
        window.location.reload();
    }

    return (
        <div className={`pop-wrpr ${serverUpdated ? '' : 'hide'}`}>
            <div className="inner-pop-wrpr">
                <div className="pop-header-wrpr">
                    <div className="pop-hd-ttl-text">
                        Mises-à-jour
                    </div>
                    <div className="close-pop-ic-wrp" onClick={closePopup}>
                        <svg viewBox="0 0 320.591 320.591"><path d="M30.391 318.583a30.37 30.37 0 0 1-21.56-7.288c-11.774-11.844-11.774-30.973 0-42.817L266.643 10.665c12.246-11.459 31.462-10.822 42.921 1.424 10.362 11.074 10.966 28.095 1.414 39.875L51.647 311.295a30.366 30.366 0 0 1-21.256 7.288z"/><path d="M287.9 318.583a30.37 30.37 0 0 1-21.257-8.806L8.83 51.963C-2.078 39.225-.595 20.055 12.143 9.146c11.369-9.736 28.136-9.736 39.504 0l259.331 257.813c12.243 11.462 12.876 30.679 1.414 42.922-.456.487-.927.958-1.414 1.414a30.368 30.368 0 0 1-23.078 7.288z"/></svg>
                    </div>
                </div>
                <div className="pop-content-wrpr">
                    Des mises-à-jour sont disponibles. Veuillez accepter leur mise en place.
                </div>
                <div className="pop-footer-wrpr">
                    <div className="btn-popup btn-cancel-pop" onClick={closePopup}>
                        Annuler
                    </div>
                    <div className="btn-popup btn-validate-pop" onClick={getServerUpdates}>
                        Ok
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Popup;