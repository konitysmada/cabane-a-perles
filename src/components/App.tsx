import React, { useEffect } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../app/store';
import { setInternetConnection } from '../features/internetConnectionSlice';
import { setServerUpdated } from '../features/serverUpdatedSlice';
import { setWebsiteFirstTimeLoaded } from '../features/websiteFirstTimeLoadedSlice';
import wordpressAxios from '../apis/wordpress';
import { setTutos, setIdees } from '../features/itemsSlice';
import firebase from 'firebase/app';
import 'firebase/firestore';
import '../firebase-config/db-settings';
import Header from './Header';
import NotFound from './NotFound';
import Home from './Home';
import Catalog from './Catalog';
import Popup from './Popup';
import { PageUrl } from '../enums';
import '../css/App.css';

const App: React.FC = () => {

    const dispatch = useDispatch();
    const internetConnection = useSelector((state: RootState) => state.internetConnection.value);
    const firebaseDB = firebase.firestore();
    const serverUpdated = useSelector((state: RootState) => state.serverUpdated.value);
    const tutos = useSelector((state: RootState) => state.items.tutos);
    const idees = useSelector((state: RootState) => state.items.idees);

    useEffect(() => {
        const getTutosData = async () => {
            const { data } = await wordpressAxios.get('/tuto-bijoux');
            dispatch(setTutos(data));
        }
        const getIdeesData = async () => {
            const { data } = await wordpressAxios.get('/idee-creation');
            dispatch(setIdees(data));
        }
        getTutosData();
        getIdeesData();
    }, [dispatch]);

    useEffect(() => {
        const setToOnline = () => {
            dispatch(setInternetConnection(true));
            dispatch(setWebsiteFirstTimeLoaded(true));
        }
        const setToOffline = () => {
            dispatch(setInternetConnection(false));
        }
        window.addEventListener('online', setToOnline);
        window.addEventListener('offline', setToOffline);
        return () => {
            window.removeEventListener('online', setToOnline);
            window.removeEventListener('offline', setToOffline);
        };
    }, [dispatch]);

    useEffect(() => {

        if(firebaseDB) {
            const serverUpdatesCollection = firebaseDB
            .collection('server_updates')
            .orderBy('updateDateTime')
            .limit(100)
            .onSnapshot(querySnapshot => {
                querySnapshot.docs.map(doc => ({
                    ...doc.data(),
                    id: doc.id,
                }));
                dispatch(setServerUpdated(true));
                dispatch(setWebsiteFirstTimeLoaded(false));
            });
            return serverUpdatesCollection;
        }

        return () => {
            dispatch(setServerUpdated(false));
            dispatch(setWebsiteFirstTimeLoaded(true));
        };

    }, [firebaseDB, dispatch]);

    if(!internetConnection) {
        return (
            <div className="offline-custom-pg-wrpr">
                <div className="inner-offln-cstm-wrpr">
                    <div className="offln-warning-ic-wrp">
                        <svg viewBox="0 0 128 128"><path d="M57.362 26.54 20.1 91.075a7.666 7.666 0 0 0 6.639 11.5h74.518a7.666 7.666 0 0 0 6.639-11.5L70.638 26.54a7.665 7.665 0 0 0-13.276 0z" fill="#fff" data-original="#ee404c"/><g fill="#ee404c"><rect height="29.377" rx="4.333" width="9.638" x="59.181" y="46.444" data-original="#fff7ed"/><circle cx="64" cy="87.428" r="4.819" data-original="#fff7ed"/></g></svg>
                    </div>
                    <div className="offln-wrn-text">
                        Veuillez vérifier votre connexion internet
                    </div>
                </div>
            </div>
        )
    }

    return (
        <React.Fragment>
            <BrowserRouter>
                <Header />
                <div className="cntnt-cntainr inner-pddn">
                    <Switch>
                        <Redirect from="/:url*(/+)" to={window.location.pathname.slice(0, -1)} />
                        <Route path={PageUrl.HomeUrl} exact component={Home} />
                        <Route path={`${PageUrl.TutorialsUrl}/:id`}>
                            <Catalog items={tutos} />
                        </Route>
                        <Route path={`${PageUrl.IdeasUrl}/:id`}>
                            <Catalog items={idees} />
                        </Route>
                        <Route path={PageUrl.TutorialsUrl}>
                            <Catalog items={tutos} />
                        </Route>
                        <Route path={PageUrl.IdeasUrl}>
                            <Catalog items={idees} />
                        </Route>
                        <Route component={NotFound} />
                    </Switch>
                </div>
                {serverUpdated && <Popup />}
            </BrowserRouter>
        </React.Fragment>
    );
}

export default App;