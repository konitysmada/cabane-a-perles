import React, { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setPageId } from '../features/browserPathnameSlice';
import '../css/Catalog.css';
import MenuItemBlock from './MenuItemBlock';
import ItemDetails from './ItemDetails';
import { PageUrl } from '../enums';
import Loader from './Loader';

interface Props {
    items: object[];
}

interface UrlParamProps {
    id: string;
}

const Catalog: React.FC<Props> = ({ items }) => {

    const { id }: UrlParamProps = useParams();
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(setPageId(id));
    }, [dispatch, id]);

    if(items.length === 0) {
        return (
            <React.Fragment>
                <Loader />
            </React.Fragment>
        )
    }

    if(id) return <ItemDetails items={items} id={id} />;

    let pageName = '';

    switch(history.location.pathname) {
        case PageUrl.TutorialsUrl:
            pageName = 'Tutos';
            break;
        case PageUrl.IdeasUrl:
            pageName = 'Idées';
            break;
    }

    const renderData = () => {
        return (
            items.map((item: any) => {
                return (
                    <MenuItemBlock
                        key={item.id}
                        text={item.title.rendered}
                        columnType="three-col-item"
                        backgroundImage={item.yoast_head_json.og_image[0].url}
                        pagePath={`${history.location.pathname}/${item.id}`}
                    />
                );
            })
        );

    }

    return (
        <div className="catalogue-pg-wrpr">
            <div className="pg-ttle">
                {pageName}
            </div>
            <div className="pg-mnu-itm-lst-wrpr">
                {renderData()}
            </div>
        </div>
    );
}

export default Catalog;