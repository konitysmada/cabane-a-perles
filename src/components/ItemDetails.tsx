import React from 'react';
import { PageUrl } from '../enums';
import NotFound from './NotFound';
import VideoPlayer from './VideoPlayer';

interface Props {
    items: object[];
    id: string;
}

const ItemDetails: React.FC<Props> = ({ items, id }) => {

    const item: any = items.find((item: any) => Number(item.id) === Number(id));
    // this will be item.videoUrl
    const videoUrl = 'https://www.selfizee.fr/wp-content/themes/selfizee/videos/academy/1-formation-booth-selfizee.mp4';
    const pathOrigins = window.location.pathname.split('/');
    let notFoundText = '';

    switch(`/${pathOrigins[1]}`) {
        case PageUrl.TutorialsUrl:
            notFoundText = 'Tuto non existant';
            break;
        case PageUrl.IdeasUrl:
            notFoundText = "Idée non existante";
            break;
    }

    if(!item) return <NotFound text={notFoundText} />;

    const renderData = () => {
        return (
            <div className="inner-it-dtls-wrpr">
                <VideoPlayer title={item.title.rendered} videoUrl={videoUrl} />
                <div className="itm-dtls-img-wrp">
                    <img src={item.yoast_head_json.og_image[0].url} alt={item.id} />
                </div>
                <div
                    className="doc-content"
                    dangerouslySetInnerHTML={ {__html: item.content.rendered} }>
                </div>
            </div>
        )
    };

    return (
        <div className="item-details-pg-wrpr">
            {renderData()}
        </div>
    );
}

export default ItemDetails;