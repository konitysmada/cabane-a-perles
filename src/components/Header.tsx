import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../app/store';
import { setBrowserPathname } from '../features/browserPathnameSlice';
import '../css/Header.css';
import { getLogoImgUrl } from '../image-urls';
import { PageUrl } from '../enums';

const Header: React.FC = () => {

    const history = useHistory();
    const dispatch = useDispatch();
    const browserPathname = useSelector((state: RootState) => state.browserPathname.value);
    const pageId = useSelector((state: RootState) => state.browserPathname.pageId);

    useEffect(() => {
        return history.listen((location) => {
            // Scroll content container to top on url change
            const contentContainer = document.querySelector('.cntnt-cntainr')! as HTMLDListElement;
            contentContainer.scrollTo(0, 0);
            // Set browser pathname
            dispatch(setBrowserPathname(location.pathname));
            //window.location.reload();
        }); 
    }, [history, dispatch]);
     
    const goToPreviousPage = () => {
        // Programmatically set steps for back button on header
        const historyPathname = history.location.pathname;
        switch(true) {
            case historyPathname === PageUrl.TutorialsUrl:
            case historyPathname === PageUrl.IdeasUrl:
                history.push('/');
                break;
            case historyPathname === `${PageUrl.TutorialsUrl}/${pageId}`:
                history.push(PageUrl.TutorialsUrl);
                break;
            case historyPathname === `${PageUrl.IdeasUrl}/${pageId}`:
                history.push(PageUrl.IdeasUrl);
                break;
            default:
                history.goBack();
        }
     }

    return (
        <header>
            <div className="inner-hd-wrpr inner-pddn">
                <div
                    className={`bck-ic-wrp hd-item-block ${browserPathname === '/' ? 'hide' : ''}`}
                    onClick={goToPreviousPage}
                >
                    <svg viewBox="0 0 512 512"><path d="M256 0C114.615 0 0 114.615 0 256s114.615 256 256 256 256-114.615 256-256S397.385 0 256 0zm0 480C132.288 480 32 379.712 32 256S132.288 32 256 32s224 100.288 224 224-100.288 224-224 224z"/><path d="m292.64 116.8-128 128c-6.204 6.241-6.204 16.319 0 22.56l128 128 22.56-22.72L198.56 256 315.2 139.36l-22.56-22.56z"/></svg>
                </div>
                <div className="hd-logo-wrpr hd-item-block">
                    <a href="/">
                        <img src={getLogoImgUrl()} alt="logo" />
                    </a>
                </div>
                <div className="hd-srch-wrpr hd-item-block">
                    <div className="inner-hd-srch-wrpr">
                        <div className="srch-input-wrp">
                            <input type="text" placeholder="Rechercher..." autoComplete="off" />
                        </div>
                        <div className="srch-ic-wrp">
                            <svg viewBox="0 0 136 136.219"><path d="M93.148 80.832c16.352-23.09 10.883-55.062-12.207-71.41S25.88-1.461 9.531 21.632C-6.816 44.723-1.352 76.693 21.742 93.04a51.226 51.226 0 0 0 55.653 2.3l37.77 37.544c4.077 4.293 10.862 4.465 15.155.387 4.293-4.075 4.465-10.86.39-15.153a9.21 9.21 0 0 0-.39-.39Zm-41.84 3.5c-18.245.004-33.038-14.777-33.05-33.023-.004-18.246 14.777-33.04 33.027-33.047 18.223-.008 33.008 14.75 33.043 32.972.031 18.25-14.742 33.067-32.996 33.098h-.023Zm0 0"/></svg>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default Header;