import React from 'react';

interface Props {
    text?: string;
}

const NotFound: React.FC<Props> = ({ text }) => {

    if(!text) text = 'Page non existante';

    return (
        <div>{text}</div>
    )
}

export default NotFound;