import React, { useEffect, useRef } from 'react';
import ReactPlayer from 'react-player';
import Slider from '@material-ui/core/Slider';
import screenfull from 'screenfull';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../app/store';
import {
    setVideoIsPlaying,
    setPlayedTime,
    setIsManuallySeeking,
    setDisplayedTotalDuration,
    setVideoEnded,
    setVideoIsBuffering
} from '../features/videoPlayerSlice';
import '../css/VideoPlayer.css';
//import { getLogoImgUrl } from '../image-urls';
import { VideoControls } from '../enums';

interface Props {
    title: string;
    videoUrl: string;
}

const VideoPlayer: React.FC<Props> = ({ title, videoUrl }) => {

    const dispatch = useDispatch();
    //const videoSliderButton = document.querySelector('.vid-input-progress-wrp .MuiSlider-thumb') as HTMLSpanElement;
    const videoIsPlaying = useSelector((state: RootState) => state.videoPlayer.isPlaying);
    const playedTime = useSelector((state: RootState) => state.videoPlayer.playedTime);
    const isManuallySeeking = useSelector((state: RootState) => state.videoPlayer.isManuallySeeking);
    const displayedTotalDuration = useSelector((state: RootState) => state.videoPlayer.displayedTotalDuration);
    const videoEnded = useSelector((state: RootState) => state.videoPlayer.videoEnded);
    const videoIsBuffering = useSelector((state: RootState) => state.videoPlayer.videoIsBuffering);
    const videoPlayerRef: any = useRef(0);
    const playerControlWrapperRef: any = useRef(null);
    const middleControlsRef: any = useRef(null);
    const bottomControlsRef: any = useRef(null);

    useEffect(() => {

        dispatch(setPlayedTime(0));
        dispatch(setVideoIsPlaying(false));

        // Get total duration of video after component renders
        setTimeout(() => {
            const videoPlayerElement = document.querySelector('.inner-vid-plr-wrpr video') as HTMLVideoElement;
            videoPlayerElement.addEventListener('loadedmetadata', function () {
                dispatch(setDisplayedTotalDuration(formatVideoTime(videoPlayerElement.duration)));
            });
        }, 0);

    }, [dispatch]);

    const formatVideoTime = (seconds: number) => {
        if (isNaN(seconds)) {
            return '00:00';
        }
        const date = new Date(seconds * 1000);
        const hh = date.getUTCHours();
        const mm = date.getUTCMinutes().toString().padStart(2, '0');
        const ss = date.getUTCSeconds().toString().padStart(2, '0');

        if (hh) {
            return `${hh.toString().padStart(2, '0')}:${mm}:${ss}`;
        }
        return `${mm}:${ss}`;
    };

    const playingTimeInSeconds = videoPlayerRef.current ? videoPlayerRef.current.getCurrentTime() : '00:00';
    let formatedEllapsedTime = formatVideoTime(playingTimeInSeconds);

    const handleVideoPlay = () => {
        dispatch(setVideoIsPlaying(!videoIsPlaying));
    }

    const handleVideoProgress = (progress: any) => {

        /*
        if(!isManuallySeeking) {
            dispatch(setPlayedTime(progress.played));
        }
        */

        /*
        if(playedTime === 1) {
            //videoPlayerRef.current.seekTo(1000);
            //dispatch(setVideoIsPlaying(false));
            dispatch(setVideoEnded(true));
        } else {
            //dispatch(setVideoIsPlaying(true));
            dispatch(setVideoEnded(false));
        }
        */

        if(formatedEllapsedTime !== displayedTotalDuration) {
            if(!isManuallySeeking) {
                dispatch(setPlayedTime(progress.played));
            }
            dispatch(setVideoEnded(false));
        } else {
            handleVideoEnded();
        }
    }

    const handleVideoRewind = () => {
        videoPlayerRef.current.seekTo(videoPlayerRef.current.getCurrentTime() - 10);
        dispatch(setPlayedTime(playedTime - 0.0065));
    }

    const handleVideoFastForward = () => {
        videoPlayerRef.current.seekTo(videoPlayerRef.current.getCurrentTime() + 10);
        dispatch(setPlayedTime(playedTime + 0.0065));

        if(formatedEllapsedTime === displayedTotalDuration) {
            videoPlayerRef.current.seekTo(videoPlayerRef.current.getDuration());
            dispatch(setVideoEnded(true));
        }
    }

    const handleVideoSeekChange = (e: React.ChangeEvent<{}>, newValue: any) => {
        dispatch(setPlayedTime(newValue / 100));
        videoPlayerRef.current.seekTo(newValue / 100);
        if(playedTime === 1) {
            videoPlayerRef.current.seekTo(videoPlayerRef.current.getDuration());
            dispatch(setVideoEnded(true));
            dispatch(setVideoIsBuffering(false));
        }
    }

    const handleVideoSeekMouseDown = () => {
        dispatch(setIsManuallySeeking(true));
    }

    const handleVideoSeekMouseUp = () => {
        dispatch(setIsManuallySeeking(false));
        if(playedTime === 1) {
            videoPlayerRef.current.seekTo(videoPlayerRef.current.getDuration());
            dispatch(setVideoEnded(true));
            dispatch(setVideoIsBuffering(false));
        }
    }

    const handleVideoEnded = () => {
        dispatch(setVideoIsPlaying(false));
        dispatch(setVideoEnded(true));
    }

    const toggleVideoFullscreen = () => {
        screenfull.toggle(playerControlWrapperRef.current);
    }

    const renderPlayPauseControls = () => {

        if(!videoIsPlaying) {
            if(!videoEnded) {
                return (
                    <svg viewBox="0 0 263.374 263.374"><path d="m238.163 115.57-68.127-39.741a29241.192 29241.192 0 0 0-55.296-32.256L44.115 3.831C28.919-5.067 13.974 2.07 13.974 19.698v224c0 17.567 14.945 24.735 30.147 15.872l69.376-39.741c15.232-8.863 40.735-23.357 55.936-32.256l68.449-39.741c15.165-8.899 15.452-23.399.281-32.262z"/></svg>
                );
            }
            return (
                <svg className="rplayy-svg-ic" viewBox="0 0 512 512"><path d="M446.709 166.059c-4.698-7.51-14.73-9.243-21.724-4.043l-48.677 36.519c-6.094 4.585-7.793 13.023-3.926 19.6C384.73 239.156 391 261.656 391 285.02 391 359.464 330.443 422 256 422s-135-62.536-135-136.98c0-69.375 52.588-126.68 120-134.165v44.165c0 12.434 14.266 19.357 23.994 11.997l120-90c8.006-5.989 7.994-18.014 0-23.994l-120-90C255.231-4.37 241 2.626 241 15.02v45.498C123.9 68.267 31 166.001 31 285.02 31 409.093 131.928 512 256 512s225-102.907 225-226.98c0-41.982-11.865-83.115-34.291-118.961z"/></svg>
            );
        }
        return (
            <svg viewBox="0 0 512 512"><path d="M181.333 0H74.667c-17.643 0-32 14.357-32 32v448c0 17.643 14.357 32 32 32h106.667c17.643 0 32-14.357 32-32V32c-.001-17.643-14.358-32-32.001-32zM437.333 0H330.667c-17.643 0-32 14.357-32 32v448c0 17.643 14.357 32 32 32h106.667c17.643 0 32-14.357 32-32V32c-.001-17.643-14.358-32-32.001-32z"/></svg>
        );
    }

    const renderVideoLoader = () => {
        return videoIsBuffering && (
            <svg width="130" height="130" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="32" strokeWidth="5" stroke="#ffffff" strokeDasharray="50.26548245743669 50.26548245743669" fill="none" strokeLinecap="round"><animateTransform attributeName="transform" type="rotate" dur="2.127659574468085s" repeatCount="indefinite" keyTimes="0;1" values="0 50 50;360 50 50"/></circle><circle cx="50" cy="50" r="23" strokeWidth="5" stroke="#ffffff" strokeDasharray="36.12831551628262 36.12831551628262" strokeDashoffset="36.128" fill="none" strokeLinecap="round"><animateTransform attributeName="transform" type="rotate" dur="2.127659574468085s" repeatCount="indefinite" keyTimes="0;1" values="0 50 50;-360 50 50"/></circle></svg>
        )
    }

    return (
        <div className="video-player-pg-wrpr">
            <div
                className="vid-ttle-wrp"
                dangerouslySetInnerHTML={ {__html: title} }>
            </div>
            <div ref={playerControlWrapperRef} className="inner-vid-plr-wrpr">
                <ReactPlayer
                    height="100%"
                    width="100%"
                    style={{ display: "flex", justifyContent: "center", alignItems: "center" }}
                    url={videoUrl}
                    playing={videoIsPlaying}
                    loop={false}
                    controls={false}
                    muted={false}
                    // Disable picture in picture
                    config={{ file: { attributes: { disablePictureInPicture: true }}}}
                    // Disable right click
                    onContextMenu={(e: any) => e.preventDefault()}
                    ref={videoPlayerRef}
                    // Custom callbacks
                    onBuffer={() => dispatch(setVideoIsBuffering(true))}
                    onBufferEnd={() => dispatch(setVideoIsBuffering(false))}
                    onPlay={() => dispatch(setVideoIsPlaying(true))}
                    onPause={() => dispatch(setVideoIsPlaying(false))}
                    onProgress={handleVideoProgress}
                    onEnded={handleVideoEnded}
                />
                <div className={`abov-vid-wrpr ${videoIsBuffering ? 'is-buffering' : ''}`} onClick={handleVideoPlay}></div>
                {/* <div className={`vid-intro-img-wrp ${playedTime === 0 ? 'display' : ''}`}>
                    <img src={getLogoImgUrl()} alt="video intro logo" />
                </div> */}
                <div className={`vid-lder-wrpr ${playedTime === 0 && isManuallySeeking ? 'hide' : ''}`}>
                    {renderVideoLoader()}
                </div>
                <div ref={bottomControlsRef} className="bottom-vid-ctrls-wrpr">
                    <div className="vid-input-progress-wrp">
                        <Slider
                            min={0}
                            max={100}
                            value={playedTime * 100}
                            onChange={handleVideoSeekChange}
                            onMouseDown={handleVideoSeekMouseDown}
                            onChangeCommitted={handleVideoSeekMouseUp}
                            style={{ background: `linear-gradient(to right, var(--purple-blue-clr) ${Math.round(playedTime * 100)}%, var(--unfilled-vid-slider-clr) ${Math.round(playedTime * 100)}%)` }}
                            className="vid-plr-slider"
                        />
                    </div>
                    <div className="inner-btm-ctrls-wrpr">
                        <div className="time-progress-wrp">
                            <div className="vid-time-text">
                                {formatedEllapsedTime} / {displayedTotalDuration}
                            </div>
                        </div>
                        <div ref={middleControlsRef} className="mid-vid-ctrls-wrpr">
                            <div className="midl-vid-crtls-ic-wrp fwrd-rwnd-ic-wrp rewind-ic" onClick={handleVideoRewind}>
                                <svg viewBox="0 0 488.07 488.071"><path d="M238.986 244.034a19.318 19.318 0 0 1-9.654 16.723L28.958 427.954a19.29 19.29 0 0 1-19.306 0A19.318 19.318 0 0 1 0 411.23V76.832c0-6.892 3.675-13.27 9.652-16.723a19.345 19.345 0 0 1 19.31 0l200.379 167.2a19.331 19.331 0 0 1 9.645 16.725zm239.443-16.72-200.386-167.2c-2.978-1.724-6.312-2.583-9.651-2.583s-6.653.864-9.654 2.583a19.316 19.316 0 0 0-9.652 16.723v334.4c0 6.893 3.68 13.271 9.652 16.721a19.314 19.314 0 0 0 19.306 0l200.373-167.202a19.284 19.284 0 0 0 9.654-16.717 19.303 19.303 0 0 0-9.642-16.725z"/></svg>
                            </div>
                            <div
                                className={`
                                    midl-vid-crtls-ic-wrp
                                    playpause-ic-wrp
                                    ${!videoIsPlaying ? VideoControls.PlayIconClass : VideoControls.PauseIconClass}
                                `}
                                onClick={handleVideoPlay}
                            >
                                {renderPlayPauseControls()}
                            </div>
                            <div className="midl-vid-crtls-ic-wrp fwrd-rwnd-ic-wrp fastforward-ic" onClick={handleVideoFastForward}>
                                <svg viewBox="0 0 488.07 488.071"><path d="M238.986 244.034a19.318 19.318 0 0 1-9.654 16.723L28.958 427.954a19.29 19.29 0 0 1-19.306 0A19.318 19.318 0 0 1 0 411.23V76.832c0-6.892 3.675-13.27 9.652-16.723a19.345 19.345 0 0 1 19.31 0l200.379 167.2a19.331 19.331 0 0 1 9.645 16.725zm239.443-16.72-200.386-167.2c-2.978-1.724-6.312-2.583-9.651-2.583s-6.653.864-9.654 2.583a19.316 19.316 0 0 0-9.652 16.723v334.4c0 6.893 3.68 13.271 9.652 16.721a19.314 19.314 0 0 0 19.306 0l200.373-167.202a19.284 19.284 0 0 0 9.654-16.717 19.303 19.303 0 0 0-9.642-16.725z"/></svg>
                            </div>
                        </div>
                        <div className="vid-fullscrn-ic-wrp" onClick={toggleVideoFullscreen}>
                            <svg viewBox="0 0 512 512"><path d="M181.2 32a32 32 0 0 1-32 32H64v85.2a32 32 0 0 1-64 0V32A32 32 0 0 1 32 0h117.2a32 32 0 0 1 32 32zm-32 416H64v-85.2a32 32 0 0 0-64 0V480a32 32 0 0 0 32 32h117.2a32 32 0 1 0 0-64zM480 330.8a32 32 0 0 0-32 32V448h-85.2a32 32 0 1 0 0 64H480a32 32 0 0 0 32-32V362.8a32 32 0 0 0-32-32zM480 0H362.8a32 32 0 0 0 0 64H448v85.2a32 32 0 1 0 64 0V32a32 32 0 0 0-32-32z"/></svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default VideoPlayer;