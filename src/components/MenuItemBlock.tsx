import React from 'react';
import { Link } from 'react-router-dom';
import '../css/ItemDetails.css';

interface Props {
    text: string;
    columnType: string;
    backgroundImage?: string;
    pagePath: string
}

const MenuItemBlock: React.FC<Props> = ({ text, columnType, backgroundImage, pagePath }) => {

    const renderBackgroundImage = () => {
        if(backgroundImage) {
            return <img src={backgroundImage} alt="bg" />;
        }
        return null;
    }

    return (
        <Link to={pagePath} className={`mnu-itm-block ${columnType}`}>
            {renderBackgroundImage()}
            <div className="mnu-itm-txt" dangerouslySetInnerHTML={ {__html: text} }>
            </div>
        </Link>
    )
}

export default MenuItemBlock;