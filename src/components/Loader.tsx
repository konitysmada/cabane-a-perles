import React from 'react';

const Loader: React.FC = () => {
    return (
        <div className="app_lodr">
            <svg width="200" height="200" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="32" strokeWidth="8" stroke="#ff4d4d" strokeDasharray="50.26548245743669 50.26548245743669" fill="none" strokeLinecap="round"><animateTransform attributeName="transform" type="rotate" dur="2.127659574468085s" repeatCount="indefinite" keyTimes="0;1" values="0 50 50;360 50 50"/></circle><circle cx="50" cy="50" r="23" strokeWidth="8" stroke="#000f40" strokeDasharray="36.12831551628262 36.12831551628262" strokeDashoffset="36.128" fill="none" strokeLinecap="round"><animateTransform attributeName="transform" type="rotate" dur="2.127659574468085s" repeatCount="indefinite" keyTimes="0;1" values="0 50 50;-360 50 50"/></circle></svg>
        </div>
    );
};

export default Loader;